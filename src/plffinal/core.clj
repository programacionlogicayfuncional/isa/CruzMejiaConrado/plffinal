(ns plffinal.core)
(defn sustitucion [x] (map #(get {\< -2, \> 2, \^ 1,\v -1} %) x))

(defn sustitucion-invertida  [x]
  (map #(get {-2 \<, 2 \>, 1 \^, -1 \v} %) x))

(defn regresa-al-punto-de-origen? [x] 
  (= 0 (reduce + (sustitucion x))))

(defn evalue [x] 
  (reduce = (vec (map regresa-al-punto-de-origen? x))))

(defn regresan-al-punto-de-origen?
  ([& args]
   (evalue (concat () args)))
  ([] true))

(defn multList [z] (map #(* % -1) z))

(defn regreso-al-punto-de-origen [x]
  (if (regresa-al-punto-de-origen? x) () 
      (sustitucion-invertida (reverse (multList (sustitucion x))))))

(defn mismo-punto-final? [x y] (= (reduce + (sustitucion x)) (reduce + (sustitucion y))))

(defn sumar[x] (reductions + (sustitucion x)))
(defn coincidencias [x y] (if (< (count x) 1) 1 (inc (count (filter true? (map = (sumar x) (sumar y)))))))
